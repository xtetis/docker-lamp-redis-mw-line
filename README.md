# Список образов
sudo docker images
# Удалить образ
sudo docker image rm -f 75835a67d134

# Кто слушает порт
sudo netstat -ap | grep :3306
 
# Какие контейнеры запущены и удаление 
sudo docker-compose ps
sudo docker-compose rm --all


# Запусть сборку по docker-compose.yml
./run.sh

# Зайти в консоль образа
sudo docker run -it phpyiifulltestprholru_phpa /bin/bash

# Убить процесс образа
sudo docker kill 714f170e7dfe


phpmyadmin
    http://localhost:8000/
        root
        rootpassword
front
    http://localhost/


# Настройки соединения

    В /web/mw-line/config/web.php настройки redis
    
        'redis' => [
            'class' => 'yii\redis\Connection',
            'database' => 0,
            'unixSocket' => null,
            'hostname' => 'redis',
            'port' => 6379,
        ],

    В /web/mw-line/config/db-local.php

        return [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:dbname=docker_db;host=db;port=3306',
            'username' => 'root',
            'password' => 'rootpassword',
            'charset' => 'utf8',
            'attributes' => [
                //PDO::ATTR_PERSISTENT => true,
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode=''",
            ],
            // Schema cache options (for production environment)
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ];

    В базе добавим
        INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `phone`, `status`, `last_login`, `is_admin`, `is_yard`, `is_delivery`, `lang`, `col_style`) VALUES
        (20, 'xtetis', '4-I2rIgbBPsdqfc_5zs6RnaY8Pq8FTNR', '$2y$13$hV2V6/E4md.ENMtif2Bdhujb.pMwsMwQLCGErpxZHwq.w8Hu/P9EW', NULL, 'tihonenkovaleriy@gmail.com', NULL, 1, '2020-05-03 13:03:11', 1, 1, 1, 'ru', '{\"app\\\\models\\\\Yard\": {\"3\": \"78\", \"4\": \"80\", \"5\": \"159\", \"12\": \"122\"}, \"app\\\\models\\\\Delivery\": {\"5\": \"185\", \"6\": \"30\"}}');

    таким образом имеем юзера tihonenkovaleriy@gmail.com
    и пароль j3qq4...


    в файле /web/mw-line/web/index.php
        добавить в зависимости от домена дебаг

        defined('YII_DEBUG') or define('YII_DEBUG', true);
        defined('YII_ENV') or define('YII_ENV', 'dev');

    
    В /web/mw-line/web/.htaccess
        RewriteEngine On

        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d

        RewriteRule . index.php
